package ru.astarata;

import ru.astarata.jpa.EntityRepository;
import ru.astarata.jpa.model.Student;
import ru.astarata.jpa.model.Teacher;
import ru.astarata.jpa.repository.StudentRepository;
import ru.astarata.jpa.repository.TeacherRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Application {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;
        try{
            entityManagerFactory = Persistence.createEntityManagerFactory("teachers");
            entityManager = entityManagerFactory.createEntityManager();
            if(entityManager == null)
                throw new NullPointerException("entityManager is null");

            Student student1 = new Student("Иван 1-й курс");
            Student student2 = new Student("Мария 2-й курс");

            EntityRepository<Teacher> teacherRepository = new TeacherRepository(entityManager);
            Teacher teacher1 = new Teacher("Петр 1й");
            teacher1.addStudent(student1);
            teacher1.addStudent(student2);
            teacherRepository.save(teacher1);

            Teacher teacher2 = new Teacher("Екатерина 2я");
            teacher2.addStudent(student1);
            teacherRepository.save(teacher2);

            Teacher teacher3 = new Teacher("Николай 3й");
            teacher3.addStudent(student2);
            teacherRepository.save(teacher3);

            int teacherId = teacher1.getId();
            int studentId = student1.getId();

            Teacher findTeacher = teacherRepository.findById(teacherId);
            if(findTeacher != null){
                System.out.printf("Преподаватель: %s%n", findTeacher);
                if(findTeacher.getStudents().isEmpty())
                    System.out.println("У преподавателя нет студантов.");
                else {
                    System.out.println("Студенты:");
                    for (Student student : findTeacher.getStudents()) {
                        System.out.println(student);
                    }
                }
            } else
                System.out.printf("Не найден преподаватель по Id=%d%n", teacherId);
            System.out.println("===================");

            EntityRepository<Student> studentRepository = new StudentRepository(entityManager);

            Student findStudent = studentRepository.findById(studentId);
            if(findStudent != null){
                System.out.printf("Студент: %s%n", findStudent);
                if(findStudent.getTeachers().isEmpty())
                    System.out.println("У студента нет преподавателей.");
                else {
                    System.out.println("Преподаватели:");
                    for (Teacher teacher : findStudent.getTeachers()) {
                        System.out.println(teacher);
                    }
                }
            } else
                System.out.printf("Не найден студент по Id=%d%n", studentId);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(entityManager != null)
                entityManager.close();
            if(entityManagerFactory != null)
                entityManagerFactory.close();
        }



    }
}
