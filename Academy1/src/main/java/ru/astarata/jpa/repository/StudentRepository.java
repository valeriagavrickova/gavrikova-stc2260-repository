package ru.astarata.jpa.repository;

import ru.astarata.jpa.EntityRepository;
import ru.astarata.jpa.model.Student;

import javax.persistence.EntityManager;

public class StudentRepository implements EntityRepository<Student> {
    private final EntityManager entityManager;

    public StudentRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Student save(Student student) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(student);
            entityManager.getTransaction().commit();
            return student;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Student findById(Integer id) {
        return entityManager.find(Student.class, id);
    }
}
