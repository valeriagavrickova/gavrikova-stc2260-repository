package ru.astarata.jpa.repository;

import ru.astarata.jpa.EntityRepository;
import ru.astarata.jpa.model.Teacher;

import javax.persistence.EntityManager;

public class TeacherRepository implements EntityRepository<Teacher> {
    private final EntityManager entityManager;

    public TeacherRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Teacher save(Teacher teacher) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(teacher);
            entityManager.getTransaction().commit();
            return teacher;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Teacher findById(Integer id) {
        return entityManager.find(Teacher.class, id);
    }
}
