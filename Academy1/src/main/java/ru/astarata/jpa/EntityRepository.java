package ru.astarata.jpa;

public interface EntityRepository <E>{
    E save(E entity);
    E findById(Integer id);
}