package ru.astarata.jpa.model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="teachers")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "teacher_id")
    private Integer id;
    @Column(name = "name")
    private String name;
    public Teacher() {

    }

    public Teacher(String name){
        this.name = name;
    }
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(
            name = "teacher_student",
            joinColumns = {@JoinColumn(name = "teacher_id")},
            inverseJoinColumns = {@JoinColumn(name = "student_id")}
    )
    private Set<Student> students = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void addStudent(Student student){
        this.students.add(student);
        student.getTeachers().add(this);
    }

    @Override
    public String toString() {
        return String.format("%s (%d).", name, id);
    }

    @Override
    public int hashCode() {
        return 43;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(getClass() != obj.getClass())
            return false;
        Teacher teacher = (Teacher) obj;
        return id != null && id.equals(teacher.id);
    }
}
