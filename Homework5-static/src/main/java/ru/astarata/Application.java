package ru.astarata;

public class Application {
    public static void main(String[] args) {
        Integer additionSum = Math.addition(4, 5, 7, 8);
        System.out.printf("Сумма всех чисел: %d%n", additionSum);

        Integer subtractionSum = Math.subtraction(9,3,2);
        System.out.printf("Вычитание всех чисел: %d%n", subtractionSum);

        Integer maxSubtractionSum = Math.maxSubtraction(7, 3, 14);
        System.out.printf("Вычитание всех чисел из максимального: %d%n", maxSubtractionSum);

        Integer multiplicationSum = Math.multiplication(5, 5, 5);
        System.out.printf("Умножение всех чисел: %d%n", multiplicationSum);

        Double divisionSum = Math.division(400, 2, 10);
        System.out.printf("Деление всех чисел: %f%n", divisionSum);

        Integer factorial = Math.factorial(5);
        System.out.printf("Факториал числа: %d%n", factorial);
    }

}
