package ru.astarata;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Math {
    /**
     * Сложение всех чисел, которые передаются в метод. Метод возвращает результат сложения.
     * @param args
     * @return
     */
    public static int addition(int... args){
        checkArgs(args);

        return Arrays.stream(args)
                .sum();
    }

    /**
     * Вычитание всех чисел, которые передаются в метод. Метод возвращает результат вычитания.
     * @param args
     * @return
     */
    public static int subtraction(int... args){
        checkArgs(args);

        int result = args[0];
        for(int i=1; i<args.length; i++)
            result -= args[i];
        return result;
    }

    /**
     * Доп. задание: найти наибольшее число из чисел, которые передали в метод. И производитьвычитание из него.
     * @param args
     * @return
     */
    public static int maxSubtraction(int... args) {
        checkArgs(args);

        int maxValue = Arrays.stream(args).max().orElse(0);
        int sumValue = Arrays.stream(args).sum();

        return maxValue - (sumValue - maxValue);
    }

    /**
     * Умножение всех чисел, которые передаются в метод. Метод возвращает результат умножения.
     * @param args
     * @return
     */
    public static int multiplication(int... args){
        checkArgs(args);

        int result = args[0];
        for(int i=1; i<args.length; i++)
            result *= args[i];
        return result;
    }

    /**
     * 4. Деление всех чисел, которые передаются в метод. При каждом следующем делении должна идти проверка:
     * 4.1 что делимое число должно быть больше делителя. Если условие не выполнено, то метод возвращает
     * текущий результат деления.
     * 4.2 что делитель - положительное число. Если условие не выполнено, то метод возвращает текущий результат деления.
     * @param args
     * @return
     */
    public static double division(int... args){
        checkArgs(args);

        double result = args[0];
        for(int i=1; i<args.length; i++) {
            if(result < args[i])
                break;
            if(args[i] <= 0)
                break;
            result /= args[i];
        }
        return result;
    }

    /**
     * Метод, высчитывающий факториал переданного числа. Должна быть проверка, что переданноечисло положительное.
     * @param value
     * @return
     */
    public static int factorial(int value){
        if(value <= 0)
            return 0;
        int[] numbers = IntStream.rangeClosed(1, value).toArray();
        return multiplication(numbers);
    }
    public static void checkArgs(int... args) throws IllegalArgumentException{
        if(args == null || args.length == 0)
            throw new IllegalArgumentException("args");
    }
}
