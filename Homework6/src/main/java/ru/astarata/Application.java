package ru.astarata;

import java.io.Console;
import java.util.Objects;

public class Application {
    public static void main(String[] args) {
        Human wife = new Human();
        wife.setLastName("Иванова");
        wife.setFirstName("Мария");
        wife.setMiddleName("Аркадьевна");
        wife.setCity("Сочи");
        wife.setStreet("Октябарьская");
        wife.setHouse("32");
        wife.setApartment("3");
        wife.setSerialPassport("98 22");
        wife.setNumberPassport("897643");
        System.out.println(wife.toString());

        Human husband = new Human();
        husband.setLastName("Иванов");
        husband.setFirstName("Петр");
        husband.setMiddleName("Андреевич");
        husband.setCity("Сочи");
        husband.setStreet("Октябарьская");
        husband.setHouse("32");
        husband.setApartment("3");
        husband.setSerialPassport("98 23");
        husband.setNumberPassport("897646");
        System.out.println(wife.toString());

        if(Objects.equals(wife, husband))
            System.out.println("wife equal husband");
        else
            System.out.println("wife not equal husband");
    }
}
