package ru.astarata;

import java.util.Objects;

public class Human {
    private String firstName;
    private String lastName;
    private String middleName;
    private String city;
    private String street;
    private String house;
    private String apartment;
    private String serialPassport;
    private String numberPassport;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getSerialPassport() {
        return serialPassport;
    }

    public void setSerialPassport(String serial) {
        this.serialPassport = serial;
    }
    public String getNumberPassport() {
        return numberPassport;
    }

    public void setNumberPassport(String number) {
        this.numberPassport = number;
    }


    @Override
    public int hashCode() {
        int result = 0;
        if(serialPassport != null)
            result += serialPassport.hashCode();
        if(numberPassport != null)
            result += numberPassport.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof Human))
            return false;
        Human human = (Human) obj;
        return Objects.equals(this.numberPassport, human.numberPassport)
                && Objects.equals(this.serialPassport, human.serialPassport);
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();
        result.append(String.format("%s %s %s\n", lastName, firstName, middleName));
        result.append("Паспорт:\n");
        result.append(String.format("Серия: %s Номер: %s\n", serialPassport, numberPassport));
        result.append(String.format("Город %s, ул. %s, дом %s, квартира %s\n",
                city, street, house, apartment));
        return result.toString();
    }

}
