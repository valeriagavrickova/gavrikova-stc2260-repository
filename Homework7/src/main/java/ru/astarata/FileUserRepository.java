package ru.astarata;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class FileUserRepository implements UserRepository {

    private final String filename;
    private static final String DELIMITER_DATA = "\\|";
    private static final String NEW_LINE = "\r\n";

    public FileUserRepository(String filename) {
        this.filename = filename;
    }

    private Map<Integer, User> loadData() throws UserRepositoryException {

        Map<Integer, User> data = new HashMap<>();
        File file = new File(filename);
        if (!file.exists())
            return data;

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {

            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(DELIMITER_DATA);
                if (parts.length != 5)
                    throw new UserRepositoryException("Неверная структура в файле.");
                User user = new User();
                user.setId(Integer.parseInt(parts[0]));
                user.setLastName(parts[1]);
                user.setFirstName(parts[2]);
                user.setAge(Integer.parseInt(parts[3]));
                user.setHasWork(Boolean.parseBoolean(parts[4]));
                data.put(user.getId(), user);
            }
            return data;
        } catch (NullPointerException e) {
            throw new UserRepositoryException("NPE.", e);
        } catch (IOException e) {
            throw new UserRepositoryException("Ошибка работы с файлом.", e);
        } catch (NumberFormatException e) {
            throw new UserRepositoryException("Ошибка преобразования в число.", e);
        }
    }

    private void saveData(Map<Integer, User> data) throws UserRepositoryException {
        if (data == null || data.isEmpty())
            return;

        try {
            File file = new File(filename);

            if (!file.exists())
                file.createNewFile();

            try (FileWriter fw = new FileWriter(filename)) {
                for (User user : data.values()) {
                    fw.append(String.format("%s%s", user.toString(), NEW_LINE));
                }
                fw.flush();
            }
        } catch (IOException e) {
            throw new UserRepositoryException("Ошибка работы с файлом.", e);
        }
    }

    private static User innerFindById(int id, Map<Integer, User> data) throws UserRepositoryException {
        if (data == null)
            return null;
        try {
            return data.get(id);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public User findById(int id) throws UserRepositoryException {
        Map<Integer, User> dataFromDisk = loadData();
        return innerFindById(id, dataFromDisk);
    }

    @Override
    public void create(User user) throws UserRepositoryException {
        Map<Integer, User> dataFromDisk = loadData();
        User userFromFile = innerFindById(user.getId(), loadData());
        if (userFromFile != null)
            throw new UserRepositoryException(String.format("Пользователь с таким id(%d) уже существует.",user.getId()));
        dataFromDisk.put(user.getId(), user);
        saveData(dataFromDisk);
    }

    @Override
    public void update(User user) throws UserRepositoryException {
        Map<Integer, User> dataFromDisk = loadData();
        User userFromFile = innerFindById(user.getId(), loadData());
        if (userFromFile == null)
            throw new UserRepositoryException(String.format("Пользователь с таким id(%d) не существует.",user.getId()));
        dataFromDisk.put(user.getId(), user);
        saveData(dataFromDisk);
    }

    @Override
    public void delete(int id) throws UserRepositoryException {
        Map<Integer, User> dataFromDisk = loadData();
        dataFromDisk.remove(id);
        saveData(dataFromDisk);
    }
}
