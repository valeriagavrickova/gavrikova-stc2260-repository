package ru.astarata;

public class User {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
    private Boolean hasWork;

    public User() {
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getHasWork() {
        return hasWork;
    }

    public void setHasWork(Boolean hasWork) {
        this.hasWork = hasWork;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (!(obj instanceof User))
            return false;
        var user = (User) obj;
        return this.id.equals(user.id);
    }

    @Override
    public String toString() {
        return String.format("%d|%s|%s|%d|%b", id, lastName, firstName, age, hasWork);
    }
}
