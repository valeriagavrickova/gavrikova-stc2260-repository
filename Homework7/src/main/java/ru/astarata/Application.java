package ru.astarata;

public class Application {
    private static final String FILENAME = "c:\\tmp\\users.txt";
    public static void main(String[] args) {

        UserRepository userRepository = new FileUserRepository(FILENAME);

        User user1 = new User();
        user1.setId(1);
        user1.setLastName("Булыжников");
        user1.setFirstName("Иезекииль");
        user1.setAge(33);
        user1.setHasWork(true);

        User user2 = new User();
        user2.setId(2);
        user2.setLastName("Камушкин");
        user2.setFirstName("Василий");
        user2.setAge(23);
        user2.setHasWork(false);

        try {
            userRepository.create(user1);
            printSaveUser(user1);
            userRepository.create(user2);
            printSaveUser(user2);
            User user3 = userRepository.findById(1);
            user3.setFirstName("Владимир");
            user3.setAge(27);
            userRepository.update(user3);
            printUpdateUser(user3);
        }catch (UserRepositoryException e){
            System.out.println("Ошибка работы с репозиторием");
            System.out.println(e);
        }
    }

    private static void printSaveUser(User user){
        System.out.printf("Пользователь:%s сохранен в файл:%s%n", user, FILENAME);
    }
    private static void printUpdateUser(User user){
        System.out.printf("Пользователь:%s обновлен в файле:%s%n", user, FILENAME);
    }
}
