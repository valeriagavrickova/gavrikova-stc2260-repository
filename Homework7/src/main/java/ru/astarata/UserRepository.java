package ru.astarata;

public interface UserRepository {
    User findById(int id) throws UserRepositoryException;
    void create(User user) throws UserRepositoryException;
    void update(User user) throws UserRepositoryException;
    void delete(int id) throws UserRepositoryException;
}
