package ru;

public class Ellipse extends Figure{

    @Override
    public String getPerimeter() {
        double p = 2 * Math.PI * Math.sqrt((Math.pow(getX(),2)+Math.pow(getY(),2))/2);
        return String.format(String.format("Периметр Ellipse = %,.4f",p));
    }
}
