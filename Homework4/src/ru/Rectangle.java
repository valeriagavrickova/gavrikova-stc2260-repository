package ru;

public class Rectangle extends Figure{
    @Override
    public String getPerimeter() {

        int p = 2 * (getX()+getY());
        return String.format(String.format("Периметр Rectangle = %d",p));
    }
}
