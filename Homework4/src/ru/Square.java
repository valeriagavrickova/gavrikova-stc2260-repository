package ru;

public class Square extends Rectangle implements Movable{

    @Override
    public void move(int x, int y) {
        this.setX(x);
        this.setY(y);
        System.out.println(
                String.format("Объект Square перемещен в координаты (%d, %d).",
                        getX(),
                        getY()
                )
        );
    }

}
