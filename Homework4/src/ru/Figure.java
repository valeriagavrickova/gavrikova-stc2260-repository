package ru;

abstract public class Figure {
     private int x;
     private int y;
     public abstract String getPerimeter ();

     public int getX() {
          return x;
     }

     public void setX(int x) {
          this.x = x;
     }

     public int getY() {
          return y;
     }

     public void setY(int y) {
          this.y = y;
     }
}
