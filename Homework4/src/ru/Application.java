package ru;

public class Application {
    public static void main(String[] args) {
        Figure[] figures = new Figure[2];

        Figure ellipse = new Ellipse();
        ellipse.setX(1);
        ellipse.setY(3);
        figures[0] = ellipse;

        Figure rectangle = new Rectangle();
        rectangle.setX(3);
        rectangle.setY(6);
        figures[1] = rectangle;

        Movable[] movables = new Movable[2];

        Circle circle = new Circle();
        circle.setX(2);
        circle.setY(2);
        movables[0] = circle;

        Square square = new Square();
        square.setX(5);
        square.setY(4);
        movables[1] = square;

        for (Figure figure:   figures) {
            System.out.println(figure.getPerimeter());
        }

        for (Movable movable :
                movables) {
            movable.move(4,5);
        }
    }
}
