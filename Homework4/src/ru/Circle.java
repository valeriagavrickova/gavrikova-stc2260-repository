package ru;

public class Circle extends Ellipse implements Movable {

    @Override
    public void move(int x, int y) {
        this.setX(x);
        this.setY(y);
        System.out.println(
                String.format("Объект Circle перемещен в координаты (%d, %d).",
                        getX(),
                        getY()
                )
        );
    }

}
