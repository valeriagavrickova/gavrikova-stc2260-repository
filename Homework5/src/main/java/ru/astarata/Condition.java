package ru.astarata;

public interface Condition {
    boolean isOk(int number);
}
