package ru.astarata;

import java.util.Arrays;

public class Application {
    public static void main(String[] args) {
        //проверку на четность элемента
        int evenInt[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] resultEvenInt = Sequence.filter(evenInt, number -> number % 2 == 0);
        System.out.println("Четные элементы: ");
        for (int evenNumber :resultEvenInt) {
            System.out.println(evenNumber);
        }
        System.out.println();

        //проверку, является ли сумма цифр элемента четным числом.
        int sumEvenInt[] = {5, 35, 43, 88, 103};
        int[] resultSumEvenInt = Sequence.filter(sumEvenInt, number -> {
            var numbers = Integer.toString(number).toCharArray();
            int sum = 0;

            for (char c : numbers) {
                sum += c;
            }

            return sum % 2 == 0;
        });
        System.out.println("Сумма элементов числа четная: ");
        for (int evenNumber :resultSumEvenInt) {
            System.out.println(evenNumber);
        }
        System.out.println();

    }
}
