package ru.astarata;

import java.util.Arrays;

public class Sequence {
    public static int[] filter(int[] arr, Condition conditionBy){
        return Arrays.stream(arr)
                .filter(conditionBy::isOk)
                .toArray();
    }
}
